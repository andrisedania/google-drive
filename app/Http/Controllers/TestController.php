<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Hypweb\Flysystem\GoogleDrive\GoogleDriveAdapter;
use League\Flysystem\Filesystem;
use Illuminate\Support\Facades\Storage;

class TestController extends Controller
{

    public function index() {
    	$client = new \Google_Client();
    	$client->setApplicationName('Google Sheet and PHP');
    	$client->setScopes([\Google_Service_Sheets::SPREADSHEETS]);
    	$client->setAccessType('offline');
    	$client->setAuthConfig(public_path() . '/credentials.json');
    	$service = new \Google_Service_Sheets($client);
    	$spreadsheetId = "1dOG6zQq9GOgQE_cyalVMoeciWBE0Tj46-JV3i4B212w";
    	
    	$range = "congress!D2:F8";
    	$response = $service->spreadsheets_values->get($spreadsheetId, $range);
    	$values = $response->getValues();

    	if(empty($values)) {
    		print "No data found.\n";
    	}else{
    		$mask = "%10s %-10s %s\n";
    		foreach($values as $row) {
    			echo sprintf($mask, $row[2], $row[1], $row[0]);
    		}
    	}
    }

    public function create() {
    	$client = new \Google_Client();
    	$client->setApplicationName('Google Sheet and PHP');
    	$client->setScopes([\Google_Service_Sheets::SPREADSHEETS]);
    	$client->setAccessType('offline');
    	$client->setAuthConfig(public_path() . '/credentials.json');
    	$service = new \Google_Service_Sheets($client);
    	$spreadsheetId = "1dOG6zQq9GOgQE_cyalVMoeciWBE0Tj46-JV3i4B212w";
    	
    	$range = "congress";
    	$values = [
    		["This", "is", "a", "new", "row", "to", "connect", "data"]
    	];

    	$body = new \Google_Service_Sheets_ValueRange([
    		'values' => $values
    	]);

    	$params = [
    		'valueInputOption' => 'RAW'
    	];

    	$insert = [
    		"insertDataOption" => "INSERT_ROWS"
    	];

    	$result = $service->spreadsheets_values->append(
    		$spreadsheetId,
    		$range,
    		$body,
    		$params,
    		$insert
    	);
    }

    public function update() {
    	$client = new \Google_Client();
    	$client->setApplicationName('Google Sheet and PHP');
    	$client->setScopes([\Google_Service_Sheets::SPREADSHEETS]);
    	$client->setAccessType('offline');
    	$client->setAuthConfig(public_path() . '/credentials.json');
    	$service = new \Google_Service_Sheets($client);
    	$spreadsheetId = "1dOG6zQq9GOgQE_cyalVMoeciWBE0Tj46-JV3i4B212w";
    	
    	$range = "congress!E2:F2";
    	$values = [
    		["Mulder", "Schully"]
    	];

    	$body = new \Google_Service_Sheets_ValueRange([
    		'values' => $values
    	]);

    	$params = [
    		'valueInputOption' => 'RAW'
    	];

    	$result = $service->spreadsheets_values->update(
    		$spreadsheetId,
    		$range,
    		$body,
    		$params
    	);
    }

    protected function googleData($clientID, $clientSecret, $refreshToken, $folderID) {
            config(['filesystems.disks.google.clientId' => $clientID]);
            config(['filesystems.disks.google.clientSecret' => $clientSecret]);
            config(['filesystems.disks.google.refreshToken' => $refreshToken]);
            config(['filesystems.disks.google.folderId' => $folderID]);

            \Storage::extend("google", function($app, $config) {
                $client = new \Google_Client();
                // $client->setClientId("160695297306-bmppbbe5srnk56up6p9kruva083lrpvn.apps.googleusercontent.com");
                // $client->setClientSecret("bRfW9Ysn1SUN8d4PUluGpHhv");
                // $client->refreshToken("1//04xbDaHAw9X5lCgYIARAAGAQSNwF-L9IrFaU96_BPn8o-jLOjwtvyo_X__E0-MOe-ckMIydiyvfg0HGvR69aoO8dn4F8ftJl1Xdg");
                // $adapter = new GoogleDriveAdapter($service, "1icY0CmIIhrCBkH1zUsKwYQYYDTeuQwRl");
                $client->setClientId($config['clientId']);
                $client->setClientSecret($config['clientSecret']);
                $client->refreshToken($config['refreshToken']);
                $service = new \Google_Service_Drive($client);
                $adapter = new GoogleDriveAdapter($service, $config['folderId']);

                return new Filesystem($adapter);
            });
    }

    public function googleUpload(Request $request) {
        $data = $request->all();

        $user = auth()->user();

        $this->googleData($user['client_id'], $user['client_secret'], $user['refresh_token'], $data['folderID']);
        $img = $request->file('filename'); 
                \Storage::disk('google')->put($img->getClientOriginalName().'', fopen($img, 'r+'));
                $url = \Storage::disk('google')->url($img->getClientOriginalName().'');

        //         $post->img = $url;
        //         $post->save();

        return response()->json([
            "statusCode" => 201,
            "message" => "Image added to google drive",
            "image_url" => $url
        ], 201);
    }

    public function gdrive() {
    	$client = new \Google_Client();
    	$client->setApplicationName('Google Drive Uploader');
    	// $client->setScopes(['https://www.googleapis.com/auth/drive']);
    	// $client->setAccessType('offline');
    	$client->setAuthConfig(public_path() . '/gdrive.json');
    	$service = new \Google_Service_Drive($client);
    	echo "<pre>";
    	print_r($service);
    	echo "</pre>";
    	die();
    }
}
