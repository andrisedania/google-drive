<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ApiController extends Controller
{
    public function hello() {
        return response()->json([
            "statusCode" => 200,
            "message" => "Hello World!"
        ], 200);
    }

    public function addRecord(Request $request) {
    	$data = $request->all();
    	$name = $data['name'];
    	$email = $data['email'];
    	$phone = $data['phone'];
    	$image_url = $data['image_url'];
        $sheetid = $data['sheetid'];
        date_default_timezone_set('Asia/Kuala_Lumpur');
    	$date = date('Y-m-d H:i:s');

    	$googlesheet = $this->createSheet($name, $email, $phone, $image_url, $date, $sheetid);


	    return response()->json([
	        "statusCode" => 201,
	        "message" => "Record successfully added to google sheet",
	        "data" => $data,
	        "result" => $googlesheet
	    ], 201);
    }

    public function createSheet($name, $email, $phone, $image_url, $date, $sheetid) {
    	$client = new \Google_Client();
    	$client->setApplicationName('Google Sheet and PHP');
    	$client->setScopes([\Google_Service_Sheets::SPREADSHEETS]);
    	$client->setAccessType('offline');
    	$client->setAuthConfig(public_path() . '/credentials.json');
    	$service = new \Google_Service_Sheets($client);
    	$spreadsheetId = $sheetid;
        // $spreadsheetId = "1mdJOjqBNOySARemStwLskVNSfKH2_G1HdSHO4FKoVQ4";
    	
    	$range = "records";
    	$values = [
    		[$name, $email, $phone, $image_url, $date]
    	];

    	$body = new \Google_Service_Sheets_ValueRange([
    		'values' => $values
    	]);

    	$params = [
    		'valueInputOption' => 'RAW'
    	];

    	$insert = [
    		"insertDataOption" => "INSERT_ROWS"
    	];

    	$result = $service->spreadsheets_values->append(
    		$spreadsheetId,
    		$range,
    		$body,
    		$params,
    		$insert
    	);

    	return $result;
    }
}
