<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::post('/upload', function (Request $request) {
   	// $filename = $request->file('filename')->store('', 'google');
   	$img = $request->file('filename'); 
   	        \Storage::disk('google')->put($img->getClientOriginalName().'', fopen($img, 'r+'));
   	        $url = \Storage::disk('google')->url($img->getClientOriginalName().'');

   	//         $post->img = $url;
   	//         $post->save();

   	return response()->json([
   	    "statusCode" => 201,
   	    "message" => "Image added to google drive",
   	    "image_url" => $url
   	], 201);
});

Route::post('/google-upload', 'TestController@googleUpload')->name('google-upload');

Route::get('/googlesheet-data', 'TestController@index');
Route::get('/googlesheet-create', 'TestController@create');
Route::get('/googlesheet-update', 'TestController@update');
Route::get('/googledrive', 'TestController@gdrive');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
