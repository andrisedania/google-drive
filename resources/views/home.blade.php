@extends('layouts.darkly')
@section('content')
    <div>

      <div class="row">
        <div class="col-lg-12">
          <div class="bs-component">
            <div class="jumbotron">
              <div class="row">
                  <div class="col-lg-2">
                      
                  </div>
                  <div class="col-lg-8">
                      <div class="card border-warning mb-3">
                          <div class="card-header">Upload File</div>
                          <div class="card-body">
                            <form action="google-upload" method="POST" enctype="multipart/form-data">
                              @csrf
                              <div class="form-group">
                                <label for="folderID">Google Drive Folder ID</label>
                                <input type="text" class="form-control" name="folderID" id="folderID" placeholder="Enter Google Drive Folder ID">
                              </div>
                                <label for="filename">Upload File</label>
                              <div class="custom-file mb-3">
                                <input type="file" class="custom-file-input" id="customFile" name="filename">
                                <label class="custom-file-label" for="customFile">Choose file to upload</label>
                              </div>
                              <div>
                                <button type="submit" class="btn btn-warning">Submit</button>
                              </div>
                            </form>
                          </div>
                        </div>
                  </div>
                  <div class="col-lg-2">
                      
                  </div>
              </div>
            </div>
          </div>
        </div>
      </div>

    </div>
@endsection