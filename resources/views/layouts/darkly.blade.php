<!DOCTYPE html>
<html lang="en">
  @include('layouts.partials.head')
  <body>
    @include('layouts.partials.topnav')

    <div class="container">
      <!-- Containers
      ================================================== -->
      @yield('content')

    </div>
    @include('layouts.partials.scripts')
  </body>
</html>
